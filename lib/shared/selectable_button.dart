import 'package:flutter/material.dart';
import 'package:stock_trader/shared/constants.dart';

class SelectableButton extends StatelessWidget {
  const SelectableButton({
    Key key,
    this.selected,
    @required this.title,
    this.onSelectionChanged,
  }) : super(key: key);

  final String title;
  final bool selected;
  final Function onSelectionChanged;

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6),
        side: BorderSide(color: (selected) ? Colors.transparent : Colors.black),
      ),
      onPressed: onSelectionChanged,
      child: Text(
        title,
        style: TextStyle(
          fontSize: 15,
          color: (selected) ? Colors.white : kTextLightColor,
        ),
      ),
      color: (selected) ? Colors.blueAccent : Colors.transparent,
      // highlightColor: Colors.transparent,
      // splashColor: Colors.transparent,
    );
  }
}
