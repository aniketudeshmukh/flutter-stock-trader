import 'package:flutter/material.dart';
// import 'dart:io' show Platform;

// final isIOS = Platform.isIOS;

const kPrimaryColor = kRedColor;
const kSecondaryColor = Color(0xFFF3F3F3);

const kTextColor = Color(0xFF272727);
const kTextLightColor = Color(0xFF777777);

const kPositiveChangeTextColor = Colors.green;
const kNegativeChangeTextColor = Colors.red;

const kRedColor = Colors.red;
const kBlueColor = Colors.blue;

const kDefaultPadding = 20.0;
const kDefaultButtonHeight = 45.0;

const kAppTitle = 'Stock Trader';

//Sandbox - Finnhub.com
const kAPIKey = 'sandbox_bulkpuf48v6prr9i4ttg';

//Prod - Finnhub.com
const kSocketAPIKey = 'btocauf48v6vfu059j3g';
