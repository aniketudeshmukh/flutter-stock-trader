import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';

part 'company.g.dart';

@HiveType(typeId: 1)
class Company extends HiveObject {
  @HiveField(0)
  String symbol;

  @HiveField(1)
  String name;

  @HiveField(2)
  String type;

  Company({@required this.symbol, @required this.name, @required this.type});

  factory Company.fromJSON(Map<String, dynamic> json) {
    return Company(
        name: json['description'], symbol: json['symbol'], type: json['type']);
  }
}
