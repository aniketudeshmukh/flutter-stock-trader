import 'package:flutter/foundation.dart';
import 'package:flutter/rendering.dart';

class Stock with ChangeNotifier, DiagnosticableTreeMixin {
  final String name;
  final String symbol;
  final String exchange;
  num price;
  num lastClosingPrice;

  num get priceChange => price - lastClosingPrice;
  num get percentChange => (price - lastClosingPrice) / lastClosingPrice;

  Stock(
      {this.name,
      this.symbol,
      this.price,
      this.exchange,
      this.lastClosingPrice});

  void updatePrice(num price) {
    this.price = price;
    notifyListeners();
  }

@override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('name', name));
    properties.add(StringProperty('symbol', symbol));
    properties.add(DoubleProperty('price', price.toDouble()));
    properties.add(StringProperty('exchange', exchange));
    properties.add(DoubleProperty('last closing price', lastClosingPrice.toDouble()));
  }
}
