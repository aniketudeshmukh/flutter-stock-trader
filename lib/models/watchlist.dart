import 'package:hive/hive.dart';
import 'company.dart';

part 'watchlist.g.dart';

@HiveType(typeId: 2)
class WatchList extends HiveObject {
  @HiveField(0)
  List<Company> list;

  WatchList(this.list);
}
