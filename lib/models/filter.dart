class Filter {
  bool showIndices = false;
  bool showNSE = false;
  bool showBSE = false;
  bool showHoldings = false;

  bool isFiltered() => showIndices || showNSE || showBSE || showHoldings;

  void toggleShowIndices() {
    showIndices = !showIndices;
  }

  void toggleShowNSE() {
    showNSE = !showNSE;
  }

  void toggleShowBSE() {
    showBSE = !showBSE;
  }

  void toggleShowHoldings() {
    showHoldings = !showHoldings;
  }

  void clear() {
    showIndices = false;
    showNSE = false;
    showBSE = false;
    showHoldings = false;
  }
}
