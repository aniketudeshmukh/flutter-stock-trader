import 'dart:async';
import 'dart:math';
import 'package:flutter/foundation.dart';
import 'package:stock_trader/models/stock.dart';
import 'package:stock_trader/models/watchlist.dart';

class StockList extends ChangeNotifier {
  List<Stock> _stocks = [];
  List<Stock> get stocks => _stocks;

  StockList() {}

  StockList.fromStocks(List<Stock> stocks) {
    _stocks = stocks;
    _startPriceAutoUpdate();
  }

  StockList.fromWatchList(WatchList watchlist) {
    watchlist.list.forEach((e) {
      _stocks.add(Stock(
          name: e.symbol, price: 100, exchange: 'NSE', lastClosingPrice: 0));
    });
    _startPriceAutoUpdate();
  }

  void _startPriceAutoUpdate() {
    Timer.periodic(Duration(milliseconds: 300), (timer) {
      _randomlyUpdateStockPrice();
    });
  }

  void addStock(Stock stock) {
    _stocks.add(stock); // new stock list
    notifyListeners();
  }

  void removeStock(Stock stock) {
    _stocks.remove(stock); // new stock list
    notifyListeners();
  }

  void _randomlyUpdateStockPrice() {
    if (stocks.isNotEmpty) {
      var size = stocks.length;

      var random = Random();
      var index = random.nextInt(size);
      var stock = stocks[index];

      var change = random.nextInt(100);

      var newPrice =
          (index % 2 == 0) ? stock.price + change : stock.price - change;

      stock.updatePrice(newPrice);
    }
  }
}
