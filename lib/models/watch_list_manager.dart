import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';
import 'package:stock_trader/models/stock.dart';
import 'package:stock_trader/models/stock_list.dart';
import 'package:stock_trader/models/watchlist.dart';

import 'package:stock_trader/models/company.dart';

class WatchListManager {
  List<WatchList> _watchlists;
  List<WatchList> get watchlists => _watchlists;

  WatchListManager() {
    load();
  }

  void load() {
    var box = Hive.box<WatchList>('Watchlist');
    _watchlists = box.values.toList();
    if (_watchlists == null || _watchlists.isEmpty) {
      _watchlists = defaultWatchLists();
      save();
    }
  }

  List<WatchList> defaultWatchLists() => [
        WatchList([
          Company(symbol: 'ASIANPAINTS', name: 'Asian Paints', type: 'EQ'),
          Company(symbol: 'SBIN', name: 'State Bank of India', type: 'EQ'),
          Company(symbol: 'TCS', name: 'Tata Consultancy Ltd', type: 'EQ'),
          Company(symbol: 'INFY', name: 'Infosys', type: 'EQ'),
          Company(symbol: 'RELIANCE', name: 'Reliance Ltd', type: 'EQ'),
          Company(symbol: 'ICICIBANK', name: 'ICICI Bank', type: 'EQ'),
        ]),
        WatchList([]),
        WatchList([]),
        WatchList([]),
        WatchList([]),
      ];

  void save() async {
    var box = Hive.box<WatchList>('Watchlist');
    await box.clear();
    await box.addAll(_watchlists);
  }
}
