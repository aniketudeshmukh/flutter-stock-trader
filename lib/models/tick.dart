import 'package:flutter/foundation.dart';
import 'dart:convert';

class Tick {
  String symbol;
  num price;
  num volume;

  Tick({@required this.symbol, @required this.price, this.volume});

  Tick.fromJson(Map<String, dynamic> json) {
    // print(json);
    symbol = json['s'];
    price = json['p'];
    volume = json['v'];
    // print('$symbol $price $volume');
  }
}
