import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stock_trader/screens/add_stock.dart';
import 'package:stock_trader/screens/home.dart';
import 'package:stock_trader/screens/loading.dart';
import 'package:stock_trader/shared/constants.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:stock_trader/models/company.dart';
import 'dart:io' show Platform;

import 'models/watchlist.dart';

void main() async {
  await initializeDatabase();
  runApp(MyApp());
}

void initializeDatabase() async {
  await Hive.initFlutter();
  Hive.registerAdapter(CompanyAdapter());
  final companyBox = await Hive.openBox<Company>('Company');
  Hive.registerAdapter(WatchListAdapter());
  final watchlistBox = await Hive.openBox<WatchList>('Watchlist');
}

class MyApp extends StatelessWidget {
  final materialThemeData = ThemeData(
    primaryColor: Colors.red,
    textTheme: const TextTheme(
        bodyText1: TextStyle(fontSize: 15), bodyText2: TextStyle(fontSize: 13)),
  );

  final cupertinoThemeData = CupertinoThemeData(
    primaryColor: Colors.red,
  );

  Map<String, WidgetBuilder> buildRoutes(BuildContext context) => {
        Home.route: (context) => Home(),
        Loading.route: (context) => Loading(),
        AddStock.route: (context) => AddStock(),
      };

  @override
  Widget build(BuildContext context) {
    if (Platform.isIOS) {
      return CupertinoApp(
        title: kAppTitle,
        theme: cupertinoThemeData,
        initialRoute: Loading.route,
        routes: buildRoutes(context),
        localizationsDelegates: [
          DefaultMaterialLocalizations.delegate,
          DefaultWidgetsLocalizations.delegate,
          DefaultCupertinoLocalizations.delegate,
        ],
      );
    } else {
      return MaterialApp(
        title: kAppTitle,
        theme: materialThemeData,
        initialRoute: Loading.route,
        routes: buildRoutes(context),
      );
    }
  }
}
