import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:stock_trader/models/stock_list.dart';
import 'package:stock_trader/screens/market_watch/market_watch.dart';

class Wrapper extends StatelessWidget {
  const Wrapper({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: StockList(),
      child: MarketWatch(),
    );
  }
}
