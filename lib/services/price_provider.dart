import 'dart:async';

import 'package:stock_trader/models/stock.dart';

class PriceProvider {
  PriceProvider() {
    Timer.periodic(Duration(milliseconds: 300), (timer) {
      _controller.sink.add( i%2==0 ?_prices1 : _prices2);
      i++;

      if (i > 100) {
        _controller.sink.close();
        timer.cancel();
      }
     });
  }
  final _controller = StreamController<List<Stock>>();

  var i = 0;

  final _prices1 = <Stock>[
    Stock(name: 'ASIANPAINT', price: 2000.00, exchange: 'NSE', lastClosingPrice: 1900.00),
    Stock(name: 'SBIN', price: 450.00, exchange: 'NSE', lastClosingPrice: 500.00),
    Stock(name: 'LT', price: 1000.00, exchange: 'NSE', lastClosingPrice: 900.00),
    Stock(name: 'INFY', price: 1500.00, exchange: 'NSE', lastClosingPrice: 1900.00),
    Stock(name: 'TCS', price: 2400.00, exchange: 'NSE', lastClosingPrice: 2300.00),
    Stock(name: 'ICICIBANK', price: 400.00, exchange: 'NSE', lastClosingPrice: 555.00),
  ];

final _prices2 = <Stock>[
    Stock(name: 'ASIANPAINT', price: 1900.00, exchange: 'NSE', lastClosingPrice: 2000.00),
    Stock(name: 'SBIN', price: 500.00, exchange: 'NSE', lastClosingPrice: 450.00),
    Stock(name: 'LT', price: 900.00, exchange: 'NSE', lastClosingPrice: 1000.00),
    Stock(name: 'INFY', price: 1900.00, exchange: 'NSE', lastClosingPrice: 1500.00),
    Stock(name: 'TCS', price: 2300.00, exchange: 'NSE', lastClosingPrice: 2400.00),
    Stock(name: 'ICICIBANK', price: 555.00, exchange: 'NSE', lastClosingPrice: 400.00),
    Stock(name: 'ASHOKLEY', price: 555.00, exchange: 'NSE', lastClosingPrice: 400.00),
  ];


  Stream<List<Stock>> get stream => _controller.stream;

}