import 'package:stock_trader/models/company.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:hive/hive.dart';
import 'package:stock_trader/shared/constants.dart';

class CompanyMaster {
  List<Company> _companies;
  final _box = Hive.box<Company>('Company');

  CompanyMaster() {
    _getCompanyList();
  }

  void _getCompanyList() async {
    _companies = _getCompanyListFromLocal();
    print('local companies: $_companies');
    if (_companies == null || _companies.isEmpty) {
      _companies = await _getCompanyListFromServer();
      print('server companies: $_companies');
      _saveCompanyList();
    }
  }

  void _saveCompanyList() async {
    await _box.clear();
    await _box.addAll(_companies);
    print('saved list ${_box.values}');
  }

  Future<List<Company>> _getCompanyListFromServer() async {
    var response = await http.get(
        'https://finnhub.io/api/v1/stock/symbol?exchange=NS&token=$kAPIKey');
    print(response.statusCode);
    print(response.body);
    var json = jsonDecode(response.body);
    if (json == null) {
      return null;
    }
    var companies = <Company>[];
    for (Map<String, dynamic> stock in json) {
      var company = Company.fromJSON(stock);
      companies.add(company);
    }
    return companies;
  }

  List<Company> _getCompanyListFromLocal() {
    print('Read Values ${_box.values}');
    return _box.values.toList();
  }

  List<Company> searchCompany({String searchTerm}) {
    if (searchTerm == null) {
      return _companies;
    } else {
      var lowerCaseSearchTerm = searchTerm.toLowerCase();
      var filteredCompanies = _companies.where((company) {
        return company.name.toLowerCase().contains(lowerCaseSearchTerm) ||
            company.symbol.toLowerCase().contains(lowerCaseSearchTerm);
      }).toList();
      return filteredCompanies;
    }
  }
}
