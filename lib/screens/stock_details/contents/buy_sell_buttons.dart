import 'package:flutter/material.dart';
import 'package:stock_trader/shared/constants.dart';
import 'package:stock_trader/shared/primary_button.dart';

class BuySellButtons extends StatelessWidget {
  const BuySellButtons({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(kDefaultPadding),
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Expanded(
            child: PrimaryButton(
              title: 'BUY',
              color: kBlueColor,
              onPressed: () {},
            ),
          ),
          const SizedBox(width: kDefaultPadding),
          Expanded(
            child: PrimaryButton(
              title: 'SELL',
              color: kRedColor,
              onPressed: () {},
            ),
          ),
        ],
      ),
    );
  }
}
