import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:stock_trader/shared/constants.dart';
import 'package:stock_trader/models/stock.dart';

class StockDetailsTitle extends StatelessWidget {
  const StockDetailsTitle({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final stock = Provider.of<Stock>(context);

    return Container(
      padding: const EdgeInsets.all(kDefaultPadding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            stock.name.toUpperCase(),
            style: const TextStyle(
              fontSize: 24,
            ),
          ),
          const SizedBox(height: kDefaultPadding / 2),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(stock.exchange),
              Text(
                stock.price.toStringAsFixed(2),
                style: const TextStyle(color: kPositiveChangeTextColor),
              ),
              Text(
                  '${stock.priceChange.toStringAsFixed(2)} (${stock.percentChange.toStringAsFixed(2)}%)'),
              const Icon(
                Icons.card_travel,
                color: kBlueColor,
              ),
              Text(
                '52',
                style: const TextStyle(color: kBlueColor),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
