import 'package:flutter/material.dart';
import 'package:stock_trader/screens/stock_details/contents/buy_sell_buttons.dart';
import 'package:stock_trader/screens/stock_details/contents/stock_details_title.dart';

class StockDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          StockDetailsTitle(),
          const Divider(),
          const BuySellButtons(),
        ],
      ),
    );
  }
}
