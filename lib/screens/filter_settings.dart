import 'package:flutter/material.dart';
import 'package:stock_trader/models/filter.dart';
import 'package:stock_trader/shared/constants.dart';
import 'package:stock_trader/shared/selectable_button.dart';

class FilterSettings extends StatefulWidget {
  @override
  _FilterSettingsState createState() => _FilterSettingsState();
}

class _FilterSettingsState extends State<FilterSettings> {
  Filter filter = Filter();

  void toggleIndices() {
    setState(() {
      filter.toggleShowIndices();
    });
  }

  void toggleNSE() {
    setState(() {
      filter.toggleShowNSE();
    });
  }

  void toggleBSE() {
    setState(() {
      filter.toggleShowBSE();
    });
  }

  void toggleShowHoldings() {
    setState(() {
      filter.toggleShowHoldings();
    });
  }

  void clearFilters() {
    setState(() {
      filter.clear();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.fromLTRB(
            kDefaultPadding, kDefaultPadding, 0, kDefaultPadding),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Filter',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                ),
                FlatButton(
                  child: const Text(
                    'CLEAR',
                    style: TextStyle(color: Colors.blueAccent),
                  ),
                  highlightColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  onPressed: () async {
                    clearFilters();
                  },
                )
              ],
            ),
            Wrap(
              spacing: kDefaultPadding / 2,
              children: [
                SelectableButton(
                  title: 'INDICES',
                  selected: filter.showIndices,
                  onSelectionChanged: toggleIndices,
                ),
                SelectableButton(
                  title: 'NSE',
                  selected: filter.showNSE,
                  onSelectionChanged: toggleNSE,
                ),
                SelectableButton(
                  title: 'BSE',
                  selected: filter.showBSE,
                  onSelectionChanged: toggleBSE,
                ),
                SelectableButton(
                  title: 'HOLDINGS',
                  selected: filter.showHoldings,
                  onSelectionChanged: toggleShowHoldings,
                ),
              ],
            ),
          ],
        ));
  }
}
