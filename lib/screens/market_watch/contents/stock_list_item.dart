import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:stock_trader/models/stock.dart';

class StockListItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final stock = Provider.of<Stock>(context, listen: false);
    final theme = Theme.of(context);
    return Container(
        padding: const EdgeInsets.all(16.0),
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    stock.name,
                    style: theme.textTheme.bodyText1,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  const SizedBox(height: 8),
                  Text(stock.exchange,
                      style: TextStyle(color: Colors.grey[500])),
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Consumer<Stock>(
                  builder: (context, stock, child) => Text(
                      stock.price.toStringAsFixed(2),
                      style: theme.textTheme.bodyText1.copyWith(
                          color: (stock.price >= stock.lastClosingPrice)
                              ? Colors.green
                              : Colors.red)),
                ),
                const SizedBox(height: 8),
                Consumer<Stock>(
                  builder: (context, stock, child) => Text(
                      "${stock.percentChange > 0 ? '+' : ''}${stock.priceChange.toStringAsFixed(2)} (${stock.percentChange > 0 ? '+' : ''}${stock.percentChange.toStringAsFixed(2)}%)"),
                )
              ],
            )
          ],
        ));
  }
}
