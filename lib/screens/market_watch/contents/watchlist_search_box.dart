import 'package:flutter/material.dart';
import 'package:stock_trader/shared/constants.dart';

class WatchlistSearchBox extends StatelessWidget {
  final Function onSearchTap;
  final Function onFilter;
  final int count;
  final int maxCount;

  const WatchlistSearchBox({
    Key key,
    @required this.onSearchTap,
    @required this.onFilter,
    @required this.count,
    @required this.maxCount,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      margin: const EdgeInsets.symmetric(
          horizontal: kDefaultPadding, vertical: kDefaultPadding / 2),
      child: GestureDetector(
        onTap: () async {
          onSearchTap();
        },
        child: Container(
          padding: const EdgeInsets.all(kDefaultPadding / 2),
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(4)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 5,
              )
            ],
          ),
          height: 50,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Icon(Icons.search, color: kPrimaryColor),
              const SizedBox(width: kDefaultPadding / 2),
              const Expanded(
                  child: Text(
                'Search & add',
                style: TextStyle(color: kTextLightColor),
              )),
              const SizedBox(width: kDefaultPadding / 2),
              Text('$count/$maxCount',
                  style: TextStyle(color: kTextLightColor)),
              const SizedBox(width: kDefaultPadding / 2),
              Container(
                width: 1,
                height: 20,
                color: kTextLightColor,
              ),
              const SizedBox(width: kDefaultPadding / 2),
              GestureDetector(
                child: const Icon(Icons.tune, color: kPrimaryColor),
                onTap: () async {
                  onFilter();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
