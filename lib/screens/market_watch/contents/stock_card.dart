import 'package:flutter/material.dart';
import 'package:stock_trader/models/stock.dart';

class StockCard extends StatelessWidget {
  const StockCard({@required this.stock});

  final Stock stock;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Card(
      margin: const EdgeInsets.all(16.0),
      elevation: 0,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(stock.name, style: theme.textTheme.bodyText1),
              Text(stock.price.toStringAsFixed(2),
                  style: theme.textTheme.bodyText1.copyWith(
                      color: (stock.price >= stock.lastClosingPrice)
                          ? Colors.green
                          : Colors.red)),
            ],
          ),
          const SizedBox(height: 8),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(stock.exchange, style: TextStyle(color: Colors.grey[500])),
              Text(
                  '${stock.priceChange.toStringAsFixed(2)} (${(stock.percentChange * 100).toStringAsFixed(2)}%)'),
            ],
          ),
        ],
      ),
    );
  }
}
