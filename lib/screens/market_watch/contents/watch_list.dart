import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:stock_trader/models/stock.dart';
import 'package:stock_trader/models/stock_list.dart';
import 'package:stock_trader/screens/market_watch/contents/stock_list_item.dart';
import 'package:stock_trader/screens/stock_details/stock_details.dart';
import 'dart:io' show Platform;

class WatchList extends StatelessWidget {
  const WatchList({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pool = Provider.of<StockList>(context);

    void _showStockDetails({@required Stock stock}) {
      if (Platform.isIOS) {
        showCupertinoModalPopup(
            context: context,
            builder: (context) {
              return ChangeNotifierProvider<Stock>.value(
                  value: stock, child: StockDetails());
            });
      } else {
        showModalBottomSheet(
            context: context,
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.vertical(top: Radius.circular(20))),
            builder: (context) {
              return ChangeNotifierProvider<Stock>.value(
                  value: stock, child: StockDetails());
            });
      }
    }

    return Container(
      color: Colors.white,
      child: ListView.separated(
        separatorBuilder: (context, index) => Divider(
          color: Colors.grey,
          height: 1,
        ),
        itemCount: pool.stocks.length + 1,
        itemBuilder: (context, index) {
          if (index == pool.stocks.length) {
            return Container();
          }
          var stock = pool.stocks[index];
          return ChangeNotifierProvider<Stock>.value(
            value: stock,
            child: GestureDetector(
              onTap: () async {
                _showStockDetails(stock: stock);
              },
              child: StockListItem(),
            ),
          );
        },
      ),
    );
  }
}
