import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:stock_trader/shared/constants.dart';

class WatchlistSelector extends StatelessWidget {
  final List<String> watchlists = [
    'Watchlist 1',
    'Watchlist 2',
    'Watchlist 3',
    'Watchlist 4',
    'Watchlist 5'
  ];

  @override
  Widget build(BuildContext context) {
    var selectedIndex = Provider.of<ValueNotifier<int>>(context);

    return Container(
      height: 30,
      color: kRedColor,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: watchlists.length,
        itemBuilder: (context, index) => buildWatchlist(index, selectedIndex),
      ),
    );
  }

  Widget buildWatchlist(int index, var selectedIndex) => GestureDetector(
        onTap: () {
          selectedIndex.value = index;
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: kDefaultPadding / 2),
          child: Column(
            children: [
              Text(
                watchlists[index],
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    color: index == selectedIndex.value
                        ? Colors.white
                        : Colors.white60),
              ),
              Container(
                margin: const EdgeInsets.only(top: kDefaultPadding / 4),
                width: 60,
                height: 2,
                color: index == selectedIndex.value
                    ? Colors.white
                    : Colors.transparent,
              )
            ],
          ),
        ),
      );
}
