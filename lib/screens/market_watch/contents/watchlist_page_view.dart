import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:stock_trader/models/stock.dart';
import 'package:stock_trader/models/stock_list.dart';
import 'package:stock_trader/models/watch_list_manager.dart';
import 'package:stock_trader/screens/market_watch/contents/watch_list.dart';

class WatchlistPageView extends StatefulWidget {
  @override
  _WatchlistPageViewState createState() => _WatchlistPageViewState();
}

class _WatchlistPageViewState extends State<WatchlistPageView> {
  final controller = PageController(initialPage: 0);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final pageIndex = Provider.of<ValueNotifier<int>>(context);
    return PageView(
      controller: controller,
      children: [
        ChangeNotifierProvider.value(
            value: StockList.fromWatchList(WatchListManager().watchlists[0]),
            child: WatchList()),
        ChangeNotifierProvider.value(
            value: StockList.fromWatchList(WatchListManager().watchlists[1]),
            child: WatchList()),
        ChangeNotifierProvider.value(
            value: StockList.fromWatchList(WatchListManager().watchlists[2]),
            child: WatchList()),
        ChangeNotifierProvider.value(
            value: StockList.fromWatchList(WatchListManager().watchlists[3]),
            child: WatchList()),
        ChangeNotifierProvider.value(
            value: StockList.fromWatchList(WatchListManager().watchlists[4]),
            child: WatchList()),
      ],
      onPageChanged: (index) {
        pageIndex.value = index;
      },
    );
  }
}
