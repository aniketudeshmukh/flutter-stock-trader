import 'package:flutter/material.dart';

class Stacked extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
          // fit: StackFit.loose,
          children: [
            Container(color: Colors.white),
            Container(color: Colors.red, height: size.height * 0.3),
            // Column(
            //   children: [
            //     WatchlistSelector(),
            //     Container(
            //       color: Colors.white70,
            //       height: 40,),
            //     Expanded(child: WatchlistPageView()),
            //   ],
            // )
          ],
        // ),
      ),
    );
  }
}
