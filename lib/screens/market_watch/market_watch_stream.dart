import 'package:flutter/material.dart';
import 'package:stock_trader/models/stock.dart';
import 'package:stock_trader/screens/market_watch/contents/stock_card.dart';
import 'package:stock_trader/services/price_provider.dart';

class MarketWatchStreamWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Market Watch'),
      ),
      body: Container(
        color: Colors.white,
        child: StreamBuilder<List<Stock>>(
            stream: PriceProvider().stream,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                final watchlist = snapshot.data;
                return ListView.builder(
                    itemCount: watchlist.length,
                    itemBuilder: (context, index) {
                      return StockCard(stock: watchlist[index]);
                    });
              } else {
                return Container();
              }
            }),
      ),
    );
  }
}
