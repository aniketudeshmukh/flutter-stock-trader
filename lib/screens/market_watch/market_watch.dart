import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:stock_trader/models/stock_list.dart';
import 'package:stock_trader/screens/add_stock.dart';
import 'package:stock_trader/screens/market_watch/contents/watch_list.dart';
import 'package:stock_trader/screens/market_watch/contents/watchlist_page_view.dart';
import 'package:stock_trader/screens/market_watch/contents/watchlist_search_box.dart';
import 'package:stock_trader/screens/market_watch/contents/watchlist_selector.dart';
import 'package:stock_trader/screens/settings.dart';
import 'dart:io' show Platform;

class MarketWatch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // final size = MediaQuery.of(context).size;

    void showSettingsMenu() {
      showModalBottomSheet(
          context: context,
          builder: (child) {
            return Settings();
          });
    }

    if (Platform.isIOS) {
      return CupertinoPageScaffold(
        navigationBar: CupertinoNavigationBar(
          middle: const Text('Market Watch',
              style: TextStyle(
                fontSize: 24,
              )),
          trailing: CupertinoButton(
            child: const Icon(Icons.expand_more),
            onPressed: () {},
          ),
        ),
        child: WatchList(),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Market Watch',
              style: TextStyle(
                fontSize: 24,
              )),
          elevation: 0,
          actions: [
            IconButton(
              icon: const Icon(Icons.expand_more),
              onPressed: () {},
            )
          ],
        ),
        body: ChangeNotifierProvider<ValueNotifier<int>>(
          create: (context) => ValueNotifier<int>(0),
          child: Column(
            children: [
              WatchlistSelector(),
              Stack(
                children: [
                  Container(
                    height: 70,
                    child: Column(
                      children: [
                        Expanded(child: Container(color: Colors.red)),
                        Expanded(child: Container(color: Colors.white)),
                      ],
                    ),
                  ),
                  WatchlistSearchBox(
                    count: 10,
                    maxCount: 50,
                    onFilter: showSettingsMenu,
                    onSearchTap: () {
                      Navigator.pushNamed(context, AddStock.route, arguments: {
                        "stocklist":
                            Provider.of<StockList>(context, listen: false)
                      });
                    },
                  ),
                ],
              ),
              Expanded(child: WatchlistPageView()),
            ],
          ),
        ),
      );
    }
  }
}
