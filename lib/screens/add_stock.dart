import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:stock_trader/models/company.dart';
import 'package:stock_trader/models/stock.dart';
import 'package:stock_trader/models/stock_list.dart';
import 'package:stock_trader/services/company_master.dart';
import 'package:stock_trader/shared/constants.dart';

class AddStock extends StatefulWidget {
  static String route = 'AddStock';
  @override
  _AddStockState createState() => _AddStockState();
}

class _AddStockState extends State<AddStock> {
  StockList stocklist;

  List<Company> companies = [];
  CompanyMaster companymaster = CompanyMaster();
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final Map<String, Object> arguments =
        ModalRoute.of(context).settings.arguments;
    stocklist = arguments['stocklist'];

    return Scaffold(
      appBar: AppBar(
        actions: [
          FlatButton(
            child: const Text('Clear'),
            onPressed: () {
              setState(() {
                controller.clear();
              });
            },
          ),
        ],
        title: TextField(
          controller: controller,
          autofocus: true,
          onChanged: (value) async {
            setState(() {
              companies = companymaster.searchCompany(searchTerm: value);
            });
          },
          decoration: const InputDecoration(
            filled: true,
            fillColor: Colors.white38,
            hintText: 'Search e.g.: Infy bse, nifty fut',
          ),
        ),
      ),
      body: Container(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: ListView.builder(
              itemCount: companies.length,
              itemBuilder: (context, index) {
                var company = companies[index];
                return ListTile(
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('${company.symbol}'),
                        Text(
                          company.name,
                          style: const TextStyle(color: Colors.grey),
                        ),
                      ],
                    ),
                    leading: Text(company.type),
                    trailing: GestureDetector(
                      onTap: () {
                        stocklist.addStock(Stock(
                            name: company.name,
                            exchange: "NSE",
                            price: 0,
                            lastClosingPrice: 0));
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          border: Border.all(color: kBlueColor),
                        ),
                        child: const Icon(Icons.add, color: kBlueColor),
                      ),
                    ));
              })),
    );
  }
}
