import 'dart:async';

import 'package:flutter/material.dart';
import 'package:stock_trader/shared/constants.dart';
import 'package:stock_trader/screens/home.dart';

class Loading extends StatefulWidget {
  static String route = 'loading';
  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {
  double loadingPercent = 0;
  Timer timer;

  @override
  void initState() {
    timer = Timer.periodic(Duration(milliseconds: 100), (timer) {
      _increaseLoadingPercent();
      if (loadingPercent >= 1) {
        timer.cancel();
        _showNextScreen();
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    timer = null;
    super.dispose();
  }

  void _increaseLoadingPercent() {
    setState(() {
      loadingPercent += 0.1;
    });
  }

  void _showNextScreen() {
    Navigator.pushReplacementNamed(context, Home.route);
    print('Replaced');
    // Navigator.pushNamed(context, '/wrapper');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(
              width: 200,
              child: Icon(
                Icons.mail,
                size: 75,
                color: kRedColor,
              ),
            ),
            Center(
              child: SizedBox(
                width: 150,
                child: LinearProgressIndicator(
                  value: loadingPercent,
                  backgroundColor: Colors.grey,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
