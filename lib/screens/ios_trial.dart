import 'package:flutter/cupertino.dart';

class IOSHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: const Text('New App'),
      ),
      child: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const Center(child: Text('Hello')),
              const SizedBox(
                height: 10,
              ),
              CupertinoButton.filled(
                child: const Text('Submit'),
                onPressed: () {},
              ),
              const SizedBox(
                height: 10,
              ),
              CupertinoSegmentedControl(
                // selectedColor: CupertinoTheme.of(context).primaryColor,
                // pressedColor: CupertinoTheme.of(context).primaryColor,
                // thumbColor: CupertinoColors.activeBlue,
                children: {
                  0: const Text('One'),
                  1: const Text('Two'),
                  2: const Text('Three'),
                },
                onValueChanged: (val) {},
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text('Autosave'),
                  const SizedBox(
                    width: 20,
                  ),
                  CupertinoSwitch(
                    onChanged: (val) {},
                    value: true,
                    activeColor: CupertinoTheme.of(context).primaryColor,
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
