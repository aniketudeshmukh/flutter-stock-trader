import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:stock_trader/screens/market_watch/market_watch.dart';
import 'package:stock_trader/wrapper.dart';
import 'package:stock_trader/screens/Orders.dart';
import 'package:stock_trader/screens/account.dart';
import 'package:stock_trader/screens/portfolio.dart';
import 'package:stock_trader/shared/constants.dart';
import 'package:stock_trader/screens/finnhub.dart';
import 'dart:io' show Platform;
import 'package:connectivity/connectivity.dart';

import 'finnhub/finnhub_new.dart';

class Home extends StatefulWidget {
  static String route = 'home';

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _currentIndex = 1;
  static final List<Widget> _tabs = [
    // Wrapper(),
    MarketWatch(),
    FinnHubNew(),
    // Orders(),
    Portfolio(),
    Account(),
  ];

  static final List<BottomNavigationBarItem> _tabItems = const [
    BottomNavigationBarItem(
      icon: Icon(Icons.bookmark),
      label: 'Watchlist',
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.book),
      label: 'Orders',
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.card_travel),
      label: 'Portfolio',
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.person),
      label: 'User',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    if (Platform.isIOS) {
      return CupertinoTabScaffold(
        tabBar: CupertinoTabBar(items: _tabItems),
        tabBuilder: (context, index) => _tabs[index],
      );
    } else {
      return Scaffold(
        body: StreamProvider.value(
          value: Connectivity().onConnectivityChanged,
          child: Column(children: [
            Expanded(child: _tabs[_currentIndex]),
            // ConnectionAwareBar(),
          ]),
        ),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          selectedFontSize: 12,
          currentIndex: _currentIndex,
          unselectedItemColor: Colors.grey,
          showUnselectedLabels: true,
          selectedItemColor: kRedColor,
          onTap: (index) {
            if (_currentIndex != index) {
              setState(() {
                _currentIndex = index;
              });
            }
          },
          items: _tabItems,
        ),
      );
    }
  }
}

class ConnectionAwareBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final status = Provider.of<ConnectivityResult>(context);
    final isOffline = (status == ConnectivityResult.none);
    print(isOffline);
    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      width: double.infinity,
      height: isOffline ? 40 : 0,
      color: Colors.grey[800],
      child: Center(
        child: const Text(
          'Please check your internet connection.',
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }
}
