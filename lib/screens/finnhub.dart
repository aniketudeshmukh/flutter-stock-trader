import 'dart:async';

import 'package:flutter/material.dart';
import 'package:stock_trader/models/company.dart';
import 'package:stock_trader/models/tick.dart';
import 'package:stock_trader/shared/constants.dart';
import 'package:web_socket_channel/io.dart';
import 'dart:convert';

class FinnHub extends StatefulWidget {
  @override
  _FinnHubState createState() => _FinnHubState();
}

class _FinnHubState extends State<FinnHub> {
  final _channel =
      IOWebSocketChannel.connect('wss://ws.finnhub.io?token=$kSocketAPIKey');
  StreamSubscription _subscription;
  List<Tick> responses = [];

  @override
  void initState() {
    super.initState();
    initWebsocket();
  }

  void initWebsocket() {
    var stocks = [
      Company(symbol: 'AAPL', name: 'Apple', type: 'EQ'),
      Company(symbol: 'AMZN', name: 'Apple', type: 'EQ')
    ];
    _subscription = _channel.stream
        .map((jsonString) => jsonDecode(jsonString))
        .where((json) => json['type'] == 'trade')
        .map((json) => Tick.fromJson(json))
        .listen(upsert);

    subscribeForStocksPrices(stocks);
  }

  void upsert(Tick tick) {
    setState(() {
      var index =
          responses.indexWhere((element) => element.symbol == tick.symbol);
      if (index >= 0) {
        responses.removeAt(index);
        responses.insert(index, tick);
      } else {
        responses.add(tick);
      }
    });
  }

  void subscribeForStocksPrices(List<Company> stocks) {
    stocks.forEach((stock) {
      _channel.sink.add('{"type":"subscribe","symbol":"${stock.symbol}"}');
      print('Subscribed for ${stock.symbol}');
    });
  }

  @override
  void dispose() {
    _subscription.cancel();
    _channel.sink.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Finnhub")),
        body: ListView.builder(
            itemCount: responses.length,
            itemBuilder: (context, index) {
              return ListTile(
                title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(child: Text(responses[index].symbol)),
                      Expanded(child: Text(responses[index].price.toString())),
                      Expanded(child: Text(responses[index].volume.toString())),
                    ]),
              );
            }));
  }
}
