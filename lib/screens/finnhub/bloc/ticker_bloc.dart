import 'dart:async';
import 'dart:convert';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:stock_trader/models/tick.dart';
import 'package:stock_trader/shared/constants.dart';
import 'package:web_socket_channel/io.dart';
import 'package:stock_trader/models/tick.dart';

part 'ticker_event.dart';
part 'ticker_state.dart';

class TickerBloc extends Bloc<TickerEvent, TickerState> {
  TickerBloc() : super(TickerInitial());

  IOWebSocketChannel _channel;
  StreamSubscription _subscription;

  @override
  Stream<TickerState> mapEventToState(TickerEvent event) async* {
    if (event is TickerSubscribe) {
      if (_channel == null) {
        yield TickerConnecting();
        await _connect();
      }
      yield TickerSubscribing();
      _subscribeForStockPrices(event.symbols);
    }

    if (event is _TickerTicked) {
      yield TickReceived(event.tick);
    }
  }

  void _connect() {
    _channel =
        IOWebSocketChannel.connect('wss://ws.finnhub.io?token=$kSocketAPIKey');
    _subscription = _channel.stream
        // .map((jsonString) => jsonDecode(jsonString))
        .map((jsonString) {
          //print(jsonString);
          return jsonDecode(jsonString);
        })
        .where((json) => json['type'] == 'trade')
        .map((json) => Tick.fromJson(json['data'][0]))
        .listen((tick) => add(_TickerTicked(tick)));
  }

  void _disconnect() {
    print('disconnected');
    _subscription.cancel();
    _channel.sink.close();
  }

  @override
  Future<void> close() {
    print('close');
    _disconnect();
    return super.close();
  }

  void _subscribeForStockPrices(List<String> stocks) {
    stocks.forEach((symbol) {
      _channel.sink.add('{"type":"subscribe","symbol":"${symbol}"}');
      print('Subscribed for ${symbol}');
    });
  }
}
