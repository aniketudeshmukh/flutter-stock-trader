part of 'ticker_bloc.dart';

abstract class TickerState {}

class TickerInitial extends TickerState {}

class TickerConnecting extends TickerState {}

class TickerConnected extends TickerState {}

class TickerDisconnected extends TickerState {}

class TickerSubscribing extends TickerState {}

class TickReceived extends TickerState {
  final Tick tick;

  TickReceived(this.tick);
}

class TickerError extends TickerState {}
