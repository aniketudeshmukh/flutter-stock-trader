part of 'ticker_bloc.dart';

abstract class TickerEvent {}

class TickerSubscribe extends TickerEvent {
  final List<String> symbols;

  TickerSubscribe(this.symbols);
}

class _TickerTicked extends TickerEvent {
  final Tick tick;

  _TickerTicked(this.tick);
}
