import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:stock_trader/models/company.dart';
import 'package:stock_trader/models/stock.dart';
import 'package:stock_trader/models/tick.dart';
import 'package:stock_trader/screens/market_watch/contents/stock_list_item.dart';
import 'package:stock_trader/shared/constants.dart';
import 'package:web_socket_channel/io.dart';
import 'dart:convert';

import 'bloc/ticker_bloc.dart';

class FinnHubNew extends StatelessWidget {
  const FinnHubNew({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(create: (_) => TickerBloc(), child: FinnHubPage());
  }
}

class FinnHubPage extends StatefulWidget {
  @override
  _FinnHubStatePage createState() => _FinnHubStatePage();
}

class _FinnHubStatePage extends State<FinnHubPage> {
  List<Stock> stocks = [];

  @override
  void initState() {
    super.initState();
    _subscribeForStockPrices();
  }

  void _subscribeForStockPrices() {
    stocks = [
      Stock(
          name: 'APPLE',
          symbol: 'AAPL',
          price: 0,
          exchange: 'NSE',
          lastClosingPrice: 0),
      Stock(
          name: 'AMAZON',
          symbol: 'AMZN',
          price: 0,
          exchange: 'NSE',
          lastClosingPrice: 0),
      Stock(
          name: 'ICICI Bank',
          symbol: 'ICICIBANK.NS',
          price: 0,
          exchange: 'NSE',
          lastClosingPrice: 0),
      Stock(
          name: 'Bajaj Finance',
          symbol: 'BAJFINANCE.NS',
          price: 0,
          exchange: 'NSE',
          lastClosingPrice: 0),
      Stock(
          name: 'Bitcoin USD',
          symbol: 'BINANCE:BTCUSDT',
          price: 0,
          exchange: 'NSE',
          lastClosingPrice: 0),
      Stock(
          name: 'Bitcoin Cash USD',
          symbol: 'BINANCE:BCCUSDT',
          price: 0,
          exchange: 'NSE',
          lastClosingPrice: 0),
      Stock(
          name: 'Etherium USD',
          symbol: 'BINANCE:ETHUSDT',
          price: 0,
          exchange: 'NSE',
          lastClosingPrice: 0),
      Stock(
          name: 'Ripple USD',
          symbol: 'BINANCE:XRPUSDT',
          price: 0,
          exchange: 'NSE',
          lastClosingPrice: 0),
      Stock(
          name: 'LiteCoin USD',
          symbol: 'BINANCE:LTCUSDT',
          price: 0,
          exchange: 'NSE',
          lastClosingPrice: 0),
    ];
    var symbols = stocks.map((e) => e.symbol).toList();
    context.read<TickerBloc>().add(TickerSubscribe(symbols));
    context.read<TickerBloc>().listen((state) {
      if (state is TickReceived) {
        var stock =
            stocks.firstWhere((element) => element.symbol == state.tick.symbol);
        stock.updatePrice(state.tick.price);
      }
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Finnhub Bloc")),
        body: ListView.builder(
            itemCount: stocks.length,
            itemBuilder: (context, index) {
              return ChangeNotifierProvider<Stock>.value(
                value: stocks[index],
                child: StockListItem(),
              );
            }));
  }
}
