import 'package:flutter/material.dart';
import 'package:stock_trader/screens/filter_settings.dart';
import 'package:stock_trader/screens/sort_settings.dart';

class Settings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(children: [
      FilterSettings(),
      const Divider(),
      SortSettings(),
    ]));
  }
}
