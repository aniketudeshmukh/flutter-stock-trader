import 'package:flutter/material.dart';
import 'package:stock_trader/shared/constants.dart';

class SortSettings extends StatelessWidget {
  const SortSettings({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(children: [
          const Text(
            'Sort',
            style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
          ),
          const Icon(Icons.cloud_upload),
        ]),
        Row(
          children: [
            const Icon(Icons.airplay),
            const SizedBox(width: kDefaultPadding / 2),
            const Text('Alphabetically'),
          ],
        ),
        const Divider(),
        Row(
          children: [
            const Icon(Icons.airplay),
            const Text('Change'),
          ],
        ),
        const Divider(),
        Row(
          children: [
            const Icon(Icons.airplay),
            const Text('Last Traded Price'),
          ],
        ),
      ],
    );
  }
}
