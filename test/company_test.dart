import 'package:test/test.dart';
import 'package:stock_trader/models/company.dart';

void main() {
  test('Test Company Constructor from JSON', () {
    var mockDescription = 'Mock Description';
    var mockSymbol = 'Mock Symbol';
    var mockType = 'Mock Type';

    var map = {
      'description': mockDescription,
      'Symbol': mockSymbol,
      'type': mockType
    };

    var c = Company.fromJSON(map);
    print(c.name);
    expect(mockDescription, equals(c.name));
    expect(mockType, equals(c.type));
    expect(mockSymbol, equals(c.symbol));
  });
}
